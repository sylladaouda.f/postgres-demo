/**
 * 
 */
package ml.daouda.postgresdemo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Daouda Sylla
 *
 */
@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class RessousceNotFoundException extends RuntimeException {

	/**
	 * Numéro de séréalisation
	 */
	private static final long serialVersionUID = 7239858528109855318L;

	public  RessousceNotFoundException( String message) {
		super(message);
	}
	
	public  RessousceNotFoundException( String message, Throwable cause) {
		super(message, cause);
	} 
}
