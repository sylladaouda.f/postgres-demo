/**
 * 
 */
package ml.daouda.postgresdemo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Daouda Sylla
 * @Date 24 mars 2019
 * @Time 12:23:34
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("ml.daouda.postgresdemo")) //Quels API documenter ?
				.paths(PathSelectors.any()) //Quels URI documenter?
				.build().apiInfo(apiInfo())
				.tags(new Tag("question", "Gestion des questions"), new Tag("reponse", "Gestion des réponses"));
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Liste des APIs disponibles").version("1.0.0").build();
	}
}