/**
 * 
 */
package ml.daouda.postgresdemo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author Daouda Sylla
 *
 */
@Entity
@Table(name = "questions")
public class Question extends AuditModel {

	/**
	 * Numéro de séréalisation
	 */
	private static final long serialVersionUID = 6724077593613580525L;

	@Id
	@GeneratedValue(generator = "question_generator")
	@SequenceGenerator(
			name = "question_generator",
			sequenceName = "question_sequence",
			initialValue = 1000
	)
	private Long id;
	
	@NotBlank
	@Size(min = 3, max = 100)
	private String titre;
	
	@Column(columnDefinition = "text")
	private String description;

	/**
	 * @return the titre
	 */
	public String getTitre() {
		return titre;
	}

	/**
	 * @param titre the titre to set
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
