/**
 * 
 */
package ml.daouda.postgresdemo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Daouda Sylla
 *
 */
@Entity
@Table(name = "reponses")
public class Reponse extends AuditModel {

	/**
	 * Numéro de persistance
	 */
	private static final long serialVersionUID = 7399668935196788744L;

	@Id
	@GeneratedValue(generator = "reponse_generator")
	@SequenceGenerator(
			name = "reponse_generator",
			sequenceName = "reponse_sequence",
			initialValue = 1000
	)
	private Long id;
	
	@Column(columnDefinition = "text")
	private String text;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "question_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private Question question;

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the question
	 */
	public Question getQuestion() {
		return question;
	}

	/**
	 * @param question the question to set
	 */
	public void setQuestion(Question question) {
		this.question = question;
	}
}
