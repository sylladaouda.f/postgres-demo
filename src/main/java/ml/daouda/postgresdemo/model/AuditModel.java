/**
 * 
 */
package ml.daouda.postgresdemo.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Daouda Sylla
 *
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
		value = { "creerA", "miseAjourA" }, allowGetters = true
)
public abstract class AuditModel implements Serializable {

	/**
	 * Numéro de séréalisation
	 */
	private static final long serialVersionUID = -6648492535043730574L;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creer_a", nullable = false, updatable = false)
	@CreatedDate
	private Date creerA;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "mise_a_jour_a", nullable = false)
	@LastModifiedDate
	private Date miseAjourA;
}
