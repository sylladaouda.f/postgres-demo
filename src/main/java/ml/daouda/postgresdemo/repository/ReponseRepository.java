/**
 * 
 */
package ml.daouda.postgresdemo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ml.daouda.postgresdemo.model.Reponse;

/**
 * @author Daouda Sylla
 *
 */
@Repository
public interface ReponseRepository extends JpaRepository<Reponse, Long> {

	List<Reponse> findByQuestionId(Long questionId);
}
