/**
 * 
 */
package ml.daouda.postgresdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ml.daouda.postgresdemo.model.Question;

/**
 * @author Daouda Sylla
 *
 */
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

}
