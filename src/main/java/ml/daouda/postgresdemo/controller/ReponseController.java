/**
 * 
 */
package ml.daouda.postgresdemo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ml.daouda.postgresdemo.exception.RessousceNotFoundException;
import ml.daouda.postgresdemo.model.Reponse;
import ml.daouda.postgresdemo.repository.QuestionRepository;
import ml.daouda.postgresdemo.repository.ReponseRepository;

/**
 * @author Daouda Sylla
 *
 */
@Api(tags = "reponse")
@RestController
public class ReponseController {

	/**
	 * reponseRepository
	 */
	@Autowired
	private ReponseRepository reponseRepository;
	
	/**
	 * questionRepository
	 */
	@Autowired
	private QuestionRepository questionRepository;
	
	
	
	
	/**
	 * recupererReponseParQuestion
	 * @param questionId
	 * @return
	 */
	@ApiOperation(value = "Récuère la liste des réponses d'une question")
	@GetMapping("/question/{questionId}/reponses")
	public List<Reponse> recupererReponseParQuestion(@PathVariable final Long questionId){
		return reponseRepository.findByQuestionId(questionId);
	}
	
	/**
	 * ajouterUneReponse
	 * @param questionId
	 * @param reponse
	 * @return
	 */
	@ApiOperation(value = "Ajoute une réponse à une question dans la BDD")
	@PostMapping("/question/{questionId}/reponse")
	public Reponse ajouterUneReponse(@PathVariable final Long questionId,
										@Valid @RequestBody Reponse reponse) {
		return questionRepository.findById(questionId)
				.map(questionTrouvee -> {
					reponse.setQuestion(questionTrouvee);
					return reponseRepository.save(reponse);
				})
				.orElseThrow(() -> new RessousceNotFoundException("Question à répondre non trouvée avec l'id " + questionId));
	}
	
	/**
	 * modifierUneReponse
	 * @param questionId
	 * @param reponseId
	 * @param reponse
	 * @return
	 */
	@ApiOperation(value = "Modifie la réponse d'une question")
	@PutMapping("/question/{questionId}/reponse/{reponseId}")
	public Reponse modifierUneReponse(@PathVariable Long questionId,
										@PathVariable Long reponseId,
										@Valid @RequestBody final Reponse reponse) {
		if(questionRepository.existsById(questionId)) {
			throw new RessousceNotFoundException("Question de la  réponse à modifer non trouvée avec l'id " + questionId);
		}
		
		return reponseRepository.findById(reponseId)
				.map(reponseTrouvee -> {
					reponseTrouvee.setText(reponse.getText());
					return reponseRepository.save(reponseTrouvee);
				})
				.orElseThrow(() -> new RessousceNotFoundException("Réponse à modifier non trouvée avec l'id " + questionId));
	}
	
	/**
	 * supprimerUneReponse
	 * @param questionId
	 * @param reponseId
	 * @return
	 */
	@ApiOperation(value = "Supprime la réponse d'une question")
	@DeleteMapping("/question/{questionId}/reponse/{reponseId}")
	public ResponseEntity<?> supprimerUneReponse(@PathVariable final Long questionId,
													@PathVariable final Long reponseId){
		if(questionRepository.existsById(questionId)) {
			throw new RessousceNotFoundException("Question de la  réponse à supprimer non trouvée avec l'id " + questionId);
		}
		
		return reponseRepository.findById(reponseId)
				.map(reponseTrouvee -> {
					reponseRepository.delete(reponseTrouvee);
					return ResponseEntity.ok().build();
				})
				.orElseThrow(() -> new RessousceNotFoundException("Réponse à supprimer non trouvée avec l'id " + questionId)); 
	}
}
