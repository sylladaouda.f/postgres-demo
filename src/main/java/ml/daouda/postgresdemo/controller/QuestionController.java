/**
 * 
 */
package ml.daouda.postgresdemo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ml.daouda.postgresdemo.exception.RessousceNotFoundException;
import ml.daouda.postgresdemo.model.Question;
import ml.daouda.postgresdemo.repository.QuestionRepository;

/**
 * @author Daouda Sylla
 *
 */
@Api(tags = "question")
@RestController
public class QuestionController {

	/**
	 * questionRepository
	 */
	@Autowired
	private QuestionRepository questionRepository;
	
	
	
	
	/**
	 * recupererQuestions
	 * @param pageable
	 * @return
	 */
	@ApiOperation(value = "Récuère la liste des questions")
	@GetMapping("/questions")
	public Page<Question> recupererQuestions(Pageable pageable){
		return questionRepository.findAll(pageable);
	}
	
	/**
	 * poserUneQuestion
	 * @param question
	 * @return
	 */
	@ApiOperation(value = "Ajoute une question dans la BDD")
	@PostMapping("/question")
	public Question poserUneQuestion(@Valid @RequestBody final Question question) {
		return questionRepository.save(question);
	}
	
	/**
	 * modifierUneQuestion
	 * @param questionId
	 * @param question
	 * @return
	 */
	@ApiOperation(value = "Modifie une question existante")
	@PutMapping("/question/{questionId}")
	public Question modifierUneQuestion(@PathVariable final Long questionId,
										@Valid @RequestBody final Question question) {
		return questionRepository.findById(questionId)
				.map(questionTrouvee -> {
					questionTrouvee.setTitre(question.getTitre());
					questionTrouvee.setDescription(question.getDescription());
					
					return questionRepository.save(questionTrouvee);
				})
				.orElseThrow(() -> new RessousceNotFoundException("Question non trouvée avec l'id " + questionId));
	}
	
	/**
	 * supprimerUneQuestion
	 * @param questionId
	 * @return
	 */
	@ApiOperation(value = "Supprime une question")
	@DeleteMapping("/question/{questionId}")
	public ResponseEntity<?> supprimerUneQuestion(@PathVariable final Long questionId){
		return questionRepository.findById(questionId)
				.map(questionTrouvee -> {
					questionRepository.delete(questionTrouvee);
					
					return ResponseEntity.ok().build();
				})
				.orElseThrow(() -> new RessousceNotFoundException("Question non trouvée avec l'id " + questionId)); 
	}
}
